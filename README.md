##Проект VelvetechTest - основное тестовое задание.

Разворачивание проекта:

- в MSSQL Server создать базу данных (например, velvetech_test)

- скопировать строку подключения

- вставить строку подключения в файл appsettings.json в ConnectionStrings->VelvetechTestContext

- выполнить команду dotnet ef database update -p VelvetechTest

Для получения доступа к API методам необходимо:

- выполнить метод Auth/token, указав логин и пароль пользователя

- из ответа с сервера скопировать значение поля "access_token" вместе со словом Bearer

- нажать кнопку Authorize

- в появившемся модальном окне в поле Value вставить скопированный токен

- нажать кнопку Authorize

- закрыть модальное окно авторизации

Пользователи для авторизации вшиты в код:

- login: admin; password: 12345

- login: user; password: 55555
	
##Проект VelvetechTest.Test - Unit тесты для проекта VelvetechTest

Разворачивание проекта:

- в MSSQL Server скопировать базу данных, созданную для VelvetechTest, задать ей название (например, velvetech_test_test)

- скопировать строку подключения

- вставить строку подключения в VelvetechTest.Test/DatabaseInitializer.cs в конструкторе DatabaseInitializer()