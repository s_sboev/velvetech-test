﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using VelvetechTest.Models;

namespace VelvetechTest.Test
{
    class DatabaseInitializer
    {
        private readonly VelvetechTestDbContext dbContext;

        public DatabaseInitializer()
        {
            // Задать строку подключения к БД
            var options = new DbContextOptionsBuilder<VelvetechTestDbContext>()
                .UseSqlServer("Data Source = (localdb)\\MSSQLLocalDB; Initial Catalog = velvetech_test_test; Integrated Security = True; Connect Timeout = 30; Encrypt = False; TrustServerCertificate = False; ApplicationIntent = ReadWrite; MultiSubnetFailover = False")
                .Options;

            this.dbContext = new VelvetechTestDbContext(options);
        }

        public VelvetechTestDbContext GetDbContext()
        {
            return dbContext;
        }

        public void Seed()
        {
            this.dbContext.Database.EnsureDeleted();
            this.dbContext.Database.EnsureCreated();
            this.dbContext.Students.AddRange(
                new Student { FirstName = "Иван", MiddleName = "Иванович", LastName = "Иванов", Gender = Gender.Male, Identifier = "IIIvanov" },
                new Student { FirstName = "Петр", MiddleName = "Петрович", LastName = "Петров", Gender = Gender.Male, Identifier = "PPPetrov" },
                new Student { FirstName = "Василий", MiddleName = "Иванович", LastName = "Пупкин", Gender = Gender.Male, Identifier = "VIPupkin" },
                new Student { FirstName = "Мария", MiddleName = "Ивановна", LastName = "Иванова", Gender = Gender.Female, Identifier = "MIIvanova" },
                new Student { FirstName = "Сергеев", MiddleName = "Сергей", LastName = "Сергеевич", Gender = Gender.Male, Identifier = "SSSergeev" }
            );

            this.dbContext.Groups.AddRange(
                new Group { Title = "g1" },
                new Group { Title = "g2" },
                new Group { Title = "g3" },
                new Group { Title = "g4" },
                new Group { Title = "g5" }
            );
            this.dbContext.SaveChanges();

            this.dbContext.GroupStudents.AddRange(
                new GroupStudent { Group = this.dbContext.Groups.Find(1), Student = this.dbContext.Students.Find(1) },
                new GroupStudent { Group = this.dbContext.Groups.Find(1), Student = this.dbContext.Students.Find(4) },
                new GroupStudent { Group = this.dbContext.Groups.Find(1), Student = this.dbContext.Students.Find(5) },
                new GroupStudent { Group = this.dbContext.Groups.Find(2), Student = this.dbContext.Students.Find(2) },
                new GroupStudent { Group = this.dbContext.Groups.Find(2), Student = this.dbContext.Students.Find(5) },
                new GroupStudent { Group = this.dbContext.Groups.Find(3), Student = this.dbContext.Students.Find(3) },
                new GroupStudent { Group = this.dbContext.Groups.Find(3), Student = this.dbContext.Students.Find(4) },
                new GroupStudent { Group = this.dbContext.Groups.Find(3), Student = this.dbContext.Students.Find(5) },
                new GroupStudent { Group = this.dbContext.Groups.Find(4), Student = this.dbContext.Students.Find(4) },
                new GroupStudent { Group = this.dbContext.Groups.Find(5), Student = this.dbContext.Students.Find(1) },
                new GroupStudent { Group = this.dbContext.Groups.Find(5), Student = this.dbContext.Students.Find(3) }
            );
            this.dbContext.SaveChanges();
        }
    }
}
