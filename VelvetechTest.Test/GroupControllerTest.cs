﻿using Xunit;
using VelvetechTest.Controllers;
using VelvetechTest.Models;
using Microsoft.AspNetCore.Mvc;

namespace VelvetechTest.Test
{
    public class GroupControllerTest
    {
        private readonly VelvetechTestDbContext dbContext;
        public GroupControllerTest()
        {
            DatabaseInitializer initializer = new DatabaseInitializer();
            dbContext = initializer.GetDbContext();
            initializer.Seed();
        }

        [Fact]
        public void GroupController_Index_Return_ObjectResult()
        {
            //Arrange
            GroupController controller = new GroupController(dbContext);
            GroupFilterModel groupFilterModel = new GroupFilterModel();

            //Act
            var result = controller.Index(groupFilterModel);

            //Assertion
            Assert.IsType<ObjectResult>(result);
        }

        [Fact]
        public void GroupController_Create_Return_CreatedResult()
        {
            //Arrange
            GroupController controller = new GroupController(dbContext);
            GroupCreateModel groupCreateModel = new GroupCreateModel()
            {
                Title = "группа 1"
            };

            //Act
            var result = controller.Create(groupCreateModel);

            //Assertion
            Assert.IsType<CreatedResult>(result);
        }

        [Fact]
        public void GroupController_Create_Return_StatusCode500()
        {
            //Arrange
            GroupController controller = new GroupController(dbContext);
            GroupCreateModel groupCreateModel = new GroupCreateModel();

            //Act
            var result = controller.Create(groupCreateModel);

            //Assertion
            Assert.IsType<StatusCodeResult>(result);
        }

        [Fact]
        public void GroupController_Edit_Return_OkObjectResult()
        {
            //Arrange
            GroupController controller = new GroupController(dbContext);
            int groupId = 2;
            GroupCreateModel groupCreateModel = new GroupCreateModel()
            {
                Title = "группа 1"
            };

            //Act
            var result = controller.Edit(groupId, groupCreateModel);

            //Assertion
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void GroupController_Edit_Return_NotFoundResult()
        {
            //Arrange
            GroupController controller = new GroupController(dbContext);
            int groupId = 22;
            GroupCreateModel groupCreateModel = new GroupCreateModel()
            {
                Title = "группа 1"
            };

            //Act
            var result = controller.Edit(groupId, groupCreateModel);

            //Assertion
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void GroupController_Delete_Return_OkResult()
        {
            //Arrange
            GroupController controller = new GroupController(dbContext);
            int groupId = 2;

            //Act
            var result = controller.Delete(groupId);

            //Assertion
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public void GroupController_Delete_Return_NotFoundResult()
        {
            //Arrange
            GroupController controller = new GroupController(dbContext);
            int groupId = 22;

            //Act
            var result = controller.Delete(groupId);

            //Assertion
            Assert.IsType<NotFoundResult>(result);
        }
    }
}
