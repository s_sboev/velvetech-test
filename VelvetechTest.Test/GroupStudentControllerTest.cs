﻿using Xunit;
using VelvetechTest.Controllers;
using VelvetechTest.Models;
using Microsoft.AspNetCore.Mvc;

namespace VelvetechTest.Test
{
    public class GroupStudentControllerTest
    {
        private readonly VelvetechTestDbContext dbContext;
        public GroupStudentControllerTest()
        {
            DatabaseInitializer initializer = new DatabaseInitializer();
            dbContext = initializer.GetDbContext();
            initializer.Seed();
        }

        [Fact]
        public void GroupStudentController_Create_Return_CreatedResult()
        {
            //Arrange
            GroupStudentController controller = new GroupStudentController(dbContext);
            int groupId = 4;
            int studentId = 1;

            //Act
            var result = controller.Create(studentId, groupId);

            //Assertion
            Assert.IsType<CreatedResult>(result);
        }

        [Fact]
        public void GroupStudentController_Create_Return_StatusCode500()
        {
            //Arrange
            GroupStudentController controller = new GroupStudentController(dbContext);
            int groupId = 4;
            int studentId = 4;

            //Act
            var result = controller.Create(studentId, groupId);

            //Assertion
            Assert.IsType<StatusCodeResult>(result);
        }

        [Theory]
        [InlineData(22, 22)]
        [InlineData(22, 1)]
        [InlineData(1, 22)]
        public void GroupStudentController_Create_Return_NotFoundResult(int groupId, int studentId)
        {
            //Arrange
            GroupStudentController controller = new GroupStudentController(dbContext);

            //Act
            var result = controller.Create(studentId, groupId);

            //Assertion
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void GroupStudent_Delete_Return_OkResult()
        {
            //Arrange
            GroupStudentController controller = new GroupStudentController(dbContext);
            int groupId = 1;
            int studentId = 1;

            //Act
            var result = controller.Delete(studentId, groupId);

            //Assertion
            Assert.IsType<OkResult>(result);
        }

        [Theory]
        [InlineData(22,22)]
        [InlineData(22,1)]
        [InlineData(1,22)]
        [InlineData(5,2)]
        public void GroupStudent_Delete_Return_NotFoundResult(int groupId, int studentId)
        {
            //Arrange
            GroupStudentController controller = new GroupStudentController(dbContext);

            //Act
            var result = controller.Delete(studentId, groupId);

            //Assertion
            Assert.IsType<NotFoundResult>(result);
        }
    }
}
