﻿using Xunit;
using VelvetechTest.Controllers;
using VelvetechTest.Models;
using Microsoft.AspNetCore.Mvc;

namespace VelvetechTest.Test
{
    public class StudentControllerTest
    {
        private readonly VelvetechTestDbContext dbContext;

        public StudentControllerTest()
        {
            DatabaseInitializer initializer = new DatabaseInitializer();
            dbContext = initializer.GetDbContext();
            initializer.Seed();
        }

        [Fact]
        public void StudentController_Index_Return_ObjectResult()
        {
            //Arrange
            StudentController controller = new StudentController(dbContext);
            StudentFilterModel studentFilterModel = new StudentFilterModel();

            //Act
            var result = controller.Index(studentFilterModel);

            //Assertion
            Assert.IsType<ObjectResult>(result);
        }

        [Fact]
        public void StudentController_Create_Return_CreatedResult()
        {
            //Arrange
            StudentController controller = new StudentController(dbContext);
            StudentCreateModel studentCreateModel = new StudentCreateModel
            {
                FirstName = "John",
                LastName = "Smith",
                Gender = Gender.Male,
                Identifier = "JSmith"
            };

            //Act
            var result = controller.Create(studentCreateModel);

            //Assertion
            Assert.IsType<CreatedResult>(result);
        }

        [Fact]
        public void StudentController_Create_Return_StatusCode500()
        {
            //Arrange
            StudentController controller = new StudentController(dbContext);
            StudentCreateModel studentCreateModel = new StudentCreateModel
            {
                FirstName = "John",
                LastName = "Smith",
                Gender = Gender.Male,
                Identifier = "IIIVanov"
            };

            //Act
            var result = controller.Create(studentCreateModel);

            //Assertion
            Assert.IsType<StatusCodeResult>(result);
        }

        [Fact]
        public void StudentController_Edit_Return_OkObjectResult()
        {
            //Arrange
            StudentController controller = new StudentController(dbContext);
            int studentId = 3;
            StudentCreateModel studentCreateModel = new StudentCreateModel
            {
                MiddleName = "Алексеевич"
            };

            //Act
            var result = controller.Edit(studentId, studentCreateModel);

            //Assertion
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void StudentController_Edit_Return_NotFoundResult()
        {
            //Arrange
            StudentController controller = new StudentController(dbContext);
            int studentId = 55;
            StudentCreateModel studentCreateModel = new StudentCreateModel
            {
                MiddleName = "Алексеевич"
            };

            //Act
            var result = controller.Edit(studentId, studentCreateModel);

            //Assertion
            Assert.IsType<NotFoundResult>(result);
        }
        [Fact]
        public void StudentController_Edit_Return_StatusCode500()
        {
            //Arrange
            StudentController controller = new StudentController(dbContext);
            int studentId = 5;
            StudentCreateModel studentCreateModel = new StudentCreateModel
            {
                Identifier = "IIIvanov"
            };

            //Act
            var result = controller.Edit(studentId, studentCreateModel);

            //Assertion
            Assert.IsType<StatusCodeResult>(result);
        }

        [Fact]
        public void StudentController_Delete_Return_OkResult()
        {
            //Arrange
            StudentController controller = new StudentController(dbContext);
            int studentId = 2;

            //Act
            var result = controller.Delete(studentId);

            //Assertion
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public void StudentController_Delete_Return_NotFoundResult()
        {
            //Arrange
            StudentController controller = new StudentController(dbContext);
            int studentId = 28;

            //Act
            var result = controller.Delete(studentId);

            //Assertion
            Assert.IsType<NotFoundResult>(result);
        }
    }
}
