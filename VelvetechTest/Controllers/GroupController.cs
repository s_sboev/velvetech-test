﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using VelvetechTest.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;

namespace VelvetechTest.Controllers
{
    [Authorize]
    [Route("api/group")]
    public class GroupController : Controller
    {
        private readonly VelvetechTestDbContext DbContext;
        public GroupController(VelvetechTestDbContext DbContext)
        {
            this.DbContext = DbContext;
        }

        // GET: Group
        /// <summary>
        /// Список групп
        /// </summary>
        /// <param name="groupFilterModel">Параметры фильтрации групп</param>
        /// <returns>Список групп</returns>
        [HttpGet]
        [Consumes(System.Net.Mime.MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Index(GroupFilterModel groupFilterModel)
        {
            try
            {
                List<Group> filterGroups = DbContext.Groups.ToList();
                if (filterGroups.Count == 0)
                {
                    return NotFound();
                }

                if (!string.IsNullOrEmpty(groupFilterModel.Title))
                {
                    filterGroups = filterGroups
                        .Where(x => x.Title.ToLower().Contains(groupFilterModel.Title.ToLower())).ToList();
                }

                if (groupFilterModel.PageItems.HasValue && groupFilterModel.PageItems.Value != 0)
                {
                    int startIndex = groupFilterModel.StartIndex.HasValue ? groupFilterModel.StartIndex.Value : 0;
                    int countItems = groupFilterModel.PageItems.Value > filterGroups.Count - startIndex ?
                        filterGroups.Count - startIndex : groupFilterModel.PageItems.Value;
                    // если индекс первого искомого элемента больше максимального индекса - вернуть пустой список
                    if (filterGroups.Count <= startIndex || countItems <= 0)
                    {
                        return new ObjectResult(new List<GroupViewModel>());
                    }
                    filterGroups = filterGroups.GetRange(startIndex, countItems);
                }

                var groups = filterGroups.Select(x => new GroupViewModel()
                {
                    Id = x.Id,
                    Title = x.Title,
                    StudentsCount = DbContext.GroupStudents.Where(y => y.GroupId == x.Id).Count()
                }).ToList();

                return new ObjectResult(groups);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        // POST: Group/Create
        /// <summary>
        /// Создать группу
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        [HttpPost("create")]
        [Consumes(System.Net.Mime.MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult Create([FromBody] GroupCreateModel group)
        {
            try
            {
                Group newGroup = new Group()
                {
                    Title = group.Title
                };
                DbContext.Groups.Add(newGroup);
                DbContext.SaveChanges();
                return Created("api/group", newGroup);
            }
            catch (System.Exception e)
            {
                return StatusCode(500);
            }
        }

        // POST: Group/Edit/5
        /// <summary>
        /// Редактировать группу
        /// </summary>
        /// <param name="id">id группы</param>
        /// <param name="group"></param>
        /// <returns></returns>
        [HttpPut("edit/{id}")]
        [Consumes(System.Net.Mime.MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult Edit(int id, [FromBody] GroupCreateModel group)
        {
            try
            {
                Group updateGroup = DbContext.Groups.Find(id);
                if (updateGroup == null)
                {
                    return NotFound();
                }

                if (!string.IsNullOrEmpty(group.Title)) {
                    updateGroup.Title = group.Title;
                }

                DbContext.Groups.Update(updateGroup);
                DbContext.SaveChanges();
                return Ok(updateGroup);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        // POST: Group/Delete/5
        /// <summary>
        /// Удалить группу
        /// </summary>
        /// <param name="id">id группы</param>
        /// <returns></returns>
        [HttpDelete("delete/{id}")]
        [Consumes(System.Net.Mime.MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult Delete(int id)
        {
            try
            {
                Group deleteGroup = DbContext.Groups.Find(id);
                if (deleteGroup == null)
                {
                    return NotFound();
                }
                DbContext.Groups.Remove(deleteGroup);
                DbContext.SaveChanges();
                return Ok();
            }
            catch
            {
                return StatusCode(500);
            }
        }
    }
}