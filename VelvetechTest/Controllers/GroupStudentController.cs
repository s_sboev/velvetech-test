﻿using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VelvetechTest.Models;
using Microsoft.AspNetCore.Authorization;

namespace VelvetechTest.Controllers
{
    [Authorize]
    [Route("api/group-student")]
    public class GroupStudentController : Controller
    {
        private readonly VelvetechTestDbContext DbContext;
        public GroupStudentController(VelvetechTestDbContext DbContext)
        {
            this.DbContext = DbContext;
        }
        // POST: GroupStudent/Create
        /// <summary>
        /// Добавить студента в группу
        /// </summary>
        /// <param name="studentId">id студента</param>
        /// <param name="groupId">id группы</param>
        /// <returns></returns>
        [HttpPost]
        [Consumes(System.Net.Mime.MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult Create(int studentId, int groupId)
        {
            try
            {
                GroupStudent newGroupStudent = new GroupStudent();
                Group group = DbContext.Groups.Find(groupId);
                if (group == null)
                {
                    return NotFound();
                }
                newGroupStudent.Group = group;

                Student student = DbContext.Students.Find(studentId);
                if (student == null)
                {
                    return NotFound();
                }
                newGroupStudent.Student = student;

                DbContext.GroupStudents.Add(newGroupStudent);
                DbContext.SaveChanges();

                return Created("api/group-student", newGroupStudent);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        // POST: GroupStudent/Delete
        /// <summary>
        /// Удалить студента из группы
        /// </summary>
        /// <param name="studentId">id студента</param>
        /// <param name="groupId">id группы</param>
        /// <returns></returns>
        [HttpDelete]
        [Consumes(System.Net.Mime.MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult Delete(int studentId, int groupId)
        {
            try
            {
                GroupStudent deleteGroupStudent = DbContext.GroupStudents
                    .FirstOrDefault(x => x.Group.Id == groupId && x.Student.Id == studentId);
                if(deleteGroupStudent == null)
                {
                    return NotFound();
                }

                DbContext.GroupStudents.Remove(deleteGroupStudent);
                DbContext.SaveChanges();

                return Ok();
            }
            catch
            {
                return StatusCode(500);
            }
        }
    }
}