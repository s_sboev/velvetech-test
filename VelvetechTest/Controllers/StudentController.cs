﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VelvetechTest.Models;
using Microsoft.AspNetCore.Authorization;

namespace VelvetechTest.Controllers
{
    [Authorize]
    [Route("api/student")]
    public class StudentController : Controller
    {
        private readonly VelvetechTestDbContext DbContext;

        public StudentController(VelvetechTestDbContext DbContext)
        {
            this.DbContext = DbContext;
        }

        // GET: Student
        /// <summary>
        /// Список студентов
        /// </summary>
        /// <param name="studentFilterModel"></param>
        /// <returns></returns>
        [HttpGet]
        [Consumes(System.Net.Mime.MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult Index(StudentFilterModel studentFilterModel)
        {
            try
            {
                List<Student> filterStudents = DbContext.Students.ToList();
                if (filterStudents.Count == 0)
                {
                    return NotFound();
                }
                if (studentFilterModel.Gender.HasValue)
                {
                    filterStudents = filterStudents
                        .Where(x => x.Gender == studentFilterModel.Gender.Value).ToList();
                }

                if (!string.IsNullOrEmpty(studentFilterModel.FullName))
                {
                    var words = studentFilterModel.FullName.Split(' ').ToList();
                    foreach (var word in words)
                    {
                        filterStudents = filterStudents
                            .Where(x =>
                                (x.LastName + " " + x.FirstName + (!string.IsNullOrEmpty(x.MiddleName) ? " " + x.MiddleName : string.Empty)).ToLower().Contains(word.ToLower()))
                            .ToList();
                    }
                }

                if (!string.IsNullOrWhiteSpace(studentFilterModel.Identifier))
                {
                    filterStudents = filterStudents
                        .Where(x => x.Identifier.ToLower().Contains(studentFilterModel.Identifier.ToLower()))
                        .ToList();
                }

                if (!string.IsNullOrEmpty(studentFilterModel.GroupTitle))
                {
                    filterStudents = filterStudents
                        .Where(x =>
                        {
                            var groups = DbContext.GroupStudents.Where(y => y.StudentId == x.Id).Select(y => y.Group).ToList();
                            foreach (var group in groups)
                            {
                                if (group.Title.ToLower().Contains(studentFilterModel.GroupTitle.ToLower()))
                                {
                                    return true;
                                }
                            }
                            return false;
                        }).ToList();
                }

                if (studentFilterModel.PageItems.HasValue && studentFilterModel.PageItems.Value != 0)
                {
                    int startIndex = studentFilterModel.StartIndex.HasValue ? studentFilterModel.StartIndex.Value : 0;
                    int countItems = studentFilterModel.PageItems.Value > filterStudents.Count - startIndex ?
                        filterStudents.Count - startIndex : studentFilterModel.PageItems.Value;
                    if (filterStudents.Count <= startIndex || countItems <= 0)
                    {
                        return new ObjectResult(new List<StudentViewModel>());
                    }
                    filterStudents = filterStudents.GetRange(startIndex, countItems);
                }

                var students = filterStudents.Select(x => new StudentViewModel()
                {
                    Id = x.Id,
                    Gender = x.Gender.ToString(),
                    FullName = x.LastName + " " + x.FirstName
                            + (!string.IsNullOrEmpty(x.MiddleName) ? " " + x.MiddleName : string.Empty),
                    Identifier = x.Identifier,
                    Groups = string.Join(',', DbContext.GroupStudents.Where(y => y.StudentId == x.Id).Select(y => y.Group.Title))
                });
                return new ObjectResult(students);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        // POST: Student/Create
        /// <summary>
        /// Создать студента
        /// </summary>
        /// <param name="student"></param>
        /// <returns></returns>
        [HttpPost]
        [Consumes(System.Net.Mime.MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult Create([FromBody] StudentCreateModel student)
        {
            try
            {
                Student newStudent = new Student()
                {
                    FirstName = student.FirstName,
                    Gender = student.Gender,
                    LastName = student.LastName,
                    MiddleName = student.MiddleName,
                    Identifier = student.Identifier
                };
                DbContext.Students.Add(newStudent);
                DbContext.SaveChanges();
                return Created("api/student", newStudent);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        // POST: Student/Edit/5
        /// <summary>
        /// Изменить студента
        /// </summary>
        /// <param name="id">id студента</param>
        /// <param name="student"></param>
        /// <returns></returns>
        [HttpPut]
        [Consumes(System.Net.Mime.MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult Edit(int id, [FromBody] StudentCreateModel student)
        {
            try
            {
                Student updateStudent = DbContext.Students.Find(id);
                if (updateStudent == null)
                {
                    return NotFound();
                }
                if (student.Gender != 0)
                {
                    updateStudent.Gender = student.Gender;
                }

                if (!string.IsNullOrEmpty(student.FirstName))
                {
                    updateStudent.FirstName = student.FirstName;
                }

                if (!string.IsNullOrEmpty(student.LastName))
                {
                    updateStudent.LastName = student.LastName;
                }

                if (!string.IsNullOrEmpty(student.MiddleName))
                {
                    updateStudent.MiddleName = student.MiddleName;
                }

                if (!string.IsNullOrEmpty(student.Identifier))
                {
                    updateStudent.Identifier = student.Identifier;
                }
                DbContext.Students.Update(updateStudent);
                DbContext.SaveChanges();

                return Ok(updateStudent);
            }
            catch (System.Exception e)
            {
                return StatusCode(500);
            }
        }

        // POST: Student/Delete/5
        /// <summary>
        /// Удалить студента
        /// </summary>
        /// <param name="id">id студента</param>
        /// <returns></returns>
        [HttpDelete]
        [Consumes(System.Net.Mime.MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult Delete(int id)
        {
            try
            {
                Student deleteStudent = DbContext.Students.Find(id);
                if(deleteStudent == null)
                {
                    return NotFound();
                }
                DbContext.Students.Remove(deleteStudent);
                DbContext.SaveChanges();
                return Ok();
            }
            catch
            {
                return StatusCode(500);
            }
        }
    }
}