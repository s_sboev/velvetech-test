﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace VelvetechTest.Models
{
    /// <summary>
    /// Перечисление возможных полов:
    /// 1 - Мужской
    /// 2 - Женский
    /// </summary>
    public enum Gender
    {
        /// <summary>
        /// Мужской
        /// </summary>
        [Display(Name = "М")]
        Male = 1,

        /// <summary>
        /// Женский
        /// </summary>
        [Display(Name = "Ж")]
        Female = 2
    }
}
