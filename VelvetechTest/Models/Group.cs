﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace VelvetechTest.Models
{
    public class Group
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; private set; }

        /// <summary>
        /// Название группы
        /// </summary>
        [Required]
        [StringLength(25)]
        public string Title { get; set; }

        /// <summary>
        /// Список привязок студентов к группам
        /// </summary>
        [JsonIgnore]
        public virtual ICollection<GroupStudent> GroupStudents { get; set; }
    }
}
