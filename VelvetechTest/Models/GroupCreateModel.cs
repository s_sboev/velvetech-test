﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VelvetechTest.Models
{
    /// <summary>
    /// Модель создания группы
    /// </summary>
    public class GroupCreateModel
    {
        /// <summary>
        /// Название группы
        /// </summary>
        public string Title { get; set; }
    }
}
