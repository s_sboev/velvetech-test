﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace VelvetechTest.Models
{
    public class GroupStudent
    {
        [Key]
        public int Id { get; private set; }

        /// <summary>
        /// Группа
        /// </summary>
        [Required]
        [ForeignKey("Group")]
        public int GroupId { get; set; }
        public virtual Group Group { get; set; }

        /// <summary>
        /// Студент
        /// </summary>
        [Required]
        [ForeignKey("Student")]
        public int StudentId { get; set; }
        public virtual Student Student { get; set; }
    }
}
