﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VelvetechTest.Models
{
    /// <summary>
    /// Тип возвращаемой информации по группам
    /// </summary>
    public class GroupViewModel
    {
        /// <summary>
        /// Id группы
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название группы
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Количество студентов в группе
        /// </summary>
        public int StudentsCount { get; set; }
    }
}
