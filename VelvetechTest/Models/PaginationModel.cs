﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VelvetechTest.Models
{
    /// <summary>
    /// Абстрактный класс для реализации пагинации
    /// </summary>
    public abstract class PaginationModel
    {
        /// <summary>
        /// Индекс первого запрашиваемого элемента
        /// </summary>
        public int? StartIndex { get; set; }

        /// <summary>
        /// Количество элементов на странице
        /// </summary>
        public int? PageItems { get; set; }
    }
}
