﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace VelvetechTest.Models
{
    public class Student
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; private set; }

        /// <summary>
        /// Пол студента: 1 - Мужской, 2 - Женский
        /// </summary>
        [Required]
        [EnumDataType(typeof(Gender))]
        public Gender Gender { get; set; }

        /// <summary>
        /// Имя студента
        /// </summary>
        [Required]
        [StringLength(40)]
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия студента
        /// </summary>
        [Required]
        [StringLength(40)]
        public string LastName { get; set; }

        /// <summary>
        /// Отчество студента
        /// </summary>
        [StringLength(60)]
        public string MiddleName { get; set; }

        /// <summary>
        /// Уникальный идентификатор студента
        /// </summary>
        [StringLength(16, MinimumLength = 6)]
        public string Identifier { get; set; }

        /// <summary>
        /// Список привязок студентов к группам
        /// </summary>
        [JsonIgnore]
        public virtual ICollection<GroupStudent> GroupStudents { get; set; }
    }
}
