﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VelvetechTest.Models
{
    /// <summary>
    /// Модель создания студента
    /// </summary>
    public class StudentCreateModel
    {
        /// <summary>
        /// Пол студента: 1 - Мужской, 2 - Женский
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// Имя студента
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия студента
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Отчество студента
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Уникальный идентификатор студента
        /// </summary>
        public string Identifier { get; set; }
    }
}
