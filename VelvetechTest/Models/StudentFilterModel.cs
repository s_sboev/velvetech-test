﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VelvetechTest.Models
{
    public class StudentFilterModel : PaginationModel
    {
        /// <summary>
        /// Пол студента (1 - мужской, 2 - женский)
        /// </summary>
        public Gender? Gender { get; set; }

        /// <summary>
        /// Полное имя студента
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Уникальный идентификатор студента
        /// </summary>
        public string Identifier { get; set; }

        /// <summary>
        /// Название группы
        /// </summary>
        public string GroupTitle { get; set; }
    }
}
