﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VelvetechTest.Models
{
    public class StudentViewModel
    {
        public int Id { get; set; }
        public string Gender { get; set; }

        public string FullName { get; set; }

        public string Identifier { get; set; }

        public string Groups { get; set; }
    }
}
