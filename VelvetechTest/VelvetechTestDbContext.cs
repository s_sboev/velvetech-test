﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VelvetechTest.Models;

namespace VelvetechTest
{
    public class VelvetechTestDbContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<GroupStudent> GroupStudents { get; set; }

        public VelvetechTestDbContext(DbContextOptions<VelvetechTestDbContext> options)
        : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Student>()
                .HasIndex(x => x.Identifier)
                .IsUnique(true);

            builder.Entity<GroupStudent>()
                .HasOne(x => x.Group)
                .WithMany(x => x.GroupStudents)
                .HasForeignKey(x => x.GroupId);

            builder.Entity<GroupStudent>()
                .HasOne(x => x.Student)
                .WithMany(x => x.GroupStudents)
                .HasForeignKey(x => x.StudentId);
            builder.Entity<GroupStudent>()
                .HasIndex(x => new { x.GroupId, x.StudentId })
                .IsUnique(true);
        }

        public DbSet<VelvetechTest.Models.User> User { get; set; }
    }
}
